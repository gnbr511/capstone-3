import AppNavbar from './components/AppNavbar'
import Home from './pages/Home'
import Register from './pages/Register'
import Login from './pages/Login'
import Logout from './pages/Logout'
import Catalog from './pages/Catalog'
import ProductView from './components/ProductView'
import ErrorPage from './pages/ErrorPage'
import Dashboard from './pages/Dashboard'
import Cart from './pages/Cart'
import Orders from './pages/Orders'
import {UserProvider} from './UserContext'
import {useState} from 'react'
import {BrowserRouter as Router, Route, Routes} from 'react-router-dom'
import './App.css'

function App() {
  const [user, setUser] = useState({
      id: null,
      isAdmin: null
  })
  const unsetUser = () => localStorage.clear()
  return (
    <>
      <UserProvider value={{user, setUser, unsetUser}}>
        <Router>
          <AppNavbar/>    
          <Routes>
            <Route path="/" element={<Home/>}/>
            <Route path="/login" element={<Login/>}/>
            <Route path="/register" element={<Register/>}/>
            <Route path="/logout" element={<Logout/>}/>
            <Route path ="/products" element={<Catalog/>}/>
            <Route path="/products/:productId" element={<ProductView/>}/>
            <Route path="/dashboard" element={<Dashboard/>}/>
            <Route path="/cart" element={<Cart/>}/>
            <Route path="/orders" element={<Orders/>}/>
            <Route path="*" element={<ErrorPage/>}/>            
          </Routes>
        </Router>
      </UserProvider>
    </>
  )
}

export default App;
