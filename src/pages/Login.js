import {Form, Button, Row, Col, Container, Card} from 'react-bootstrap'
import {useState, useEffect, useContext} from 'react'
import {Navigate, useNavigate, Link} from 'react-router-dom'
import BannerCarousel from '../components/BannerCarousel'
import UserContext from '../UserContext'
import Swal from 'sweetalert2'

export default function Login(){
	const {user, setUser} = useContext(UserContext)
	const navigate = useNavigate()
	const [email, setEmail] = useState('')
	const [password, setPassword] = useState('')
	const [isActive, setIsActive] = useState('')

	useEffect(() => {
		(email !== '' && password !== '') ?
		setIsActive(true):setIsActive(false)
	}, [email, password])

	function getUserTypeAndId(userToken){
		fetch(`${process.env.REACT_APP_API_URL}/users/details`,{
			method: 'GET',
			headers: {
				Authorization: `Bearer ${userToken}`
			}
		})
		.then((response) => response.json())
		.then((result) => {
			setUser({
				id: result._id,
				isAdmin: result.isAdmin
			})
			localStorage.setItem('id', result._id)
			localStorage.setItem('isAdmin', result.isAdmin)
			navigate('/dashboard')
		})
	}

	function login(event){
		event.preventDefault()
		fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then((response) => response.json())
		.then((result) => {
			if(result.isSuccess){
				localStorage.setItem('token', result.auth)												
				setEmail('')
				setPassword('')
				Swal.fire({
					title: 'Success',
					icon: 'success',
					confirmButtonColor: "#f0ad4e",
					text: result.message
				})
				getUserTypeAndId(result.auth)				
			}else{
				Swal.fire({
					title: 'Error',
					icon: 'error',
					confirmButtonColor: "#f0ad4e",
					text: result.message
				})
			}
		})
	}

	return(
		(localStorage.getItem('id') === null) ?
		<Container>
	      <Row className="justify-content-md-center mt-3">
	      	<Col xs lg="8" className="mt-md-2 mt-lg-5"><BannerCarousel/></Col>
	        <Col lg="4" className="mt-md-2 mt-lg-3">
	        	<Card border="dark" bg="light" text="dark">
			      <Card.Header className="bg-warning"><h3><i className="fa-solid fa-pen-to-square"></i> Login</h3></Card.Header>
			      <Card.Body>
			      	<Form onSubmit={event => login(event)}>
						<Form.Group controlId="userEmail">
			                <Form.Label>Email address</Form.Label>
			                <Form.Control 
				                type="email" 
				                placeholder="Enter email"
				                value={email}
				                onChange={event => setEmail(event.target.value)}
				                required
			                />
			                <Form.Text className="text-muted">
			                    We'll never share your email with anyone else.
			                </Form.Text>
			            </Form.Group>
			            <Form.Group controlId="password">
			                <Form.Label>Password</Form.Label>
			                <Form.Control 
				                type="password" 
				                placeholder="Password" 
				                value={password}
				                onChange={event => setPassword(event.target.value)}
				                required
			                />
			            </Form.Group>  
			            {	isActive ? 
			            	<Button variant="warning border-dark" type="submit" id="submitBtn" className="mt-3">
			            		Log in <i className="fa-solid fa-right-to-bracket"></i>
			            	</Button>
			            	:
			            	<Button variant="warning border-dark" type="submit" id="submitBtn" disabled className="mt-3">
			            		Log in <i className="fa-solid fa-right-to-bracket"></i>
			            	</Button>
			            }		            
			        </Form>
			      </Card.Body>			      
			    </Card>
			    <Link to="/register">Create an account</Link>       	
	        </Col>	   
	      </Row>     
	    </Container>
	    :
	    <Navigate to="/dashboard"/>
)	
}