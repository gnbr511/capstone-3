import {Table, Row, Col, Navbar, Nav, Container, Button} from 'react-bootstrap'
import {useState, useEffect} from 'react'
import {Navigate} from 'react-router-dom'
import Loading from '../components/Loading'
import OrderTable from '../components/OrderTable'

export default function Orders(){
	const [isLoading, setIsLoading] = useState(false)
	const [orders, setOrders] = useState([])

	const GetAllOrders = () => {
		setIsLoading(true)
		fetch(`${process.env.REACT_APP_API_URL}/orders/my-orders`,{
			method: 'GET',
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(response => response.json())
		.then(result => {
			try{
				setOrders(
					result.map(order => <OrderTable key={order._id} order={order}/>)
				)
			}catch(error){				
			}
			setIsLoading(false)
		})
	}
	useEffect(() => {
		 GetAllOrders()
	}, [])
	return (
		<Container>
			{
				(localStorage.getItem('id') !== null && localStorage.getItem('isAdmin') !== "true") ?
					<>
					<Navbar collapseOnSelect expand="lg" bg="warning" variant="warning" className="mt-2 rounded">
						<Container>
					        <Navbar.Brand><i className="fa-solid fa-clipboard-list"> My Orders</i></Navbar.Brand>
					        <Navbar.Toggle aria-controls="responsive-navbar-nav" />
					        <Navbar.Collapse id="responsive-navbar-nav">
					          <Nav className="me-auto">
					          	{/*<Button className="btn-dark" id="refresh"><i className="fa-solid fa-rotate-right"></i></Button> */}     
					          </Nav>
					          <Nav>
					            {/*<Nav.Link><i className="fa-solid fa-box-open"> My Products</i></Nav.Link>*/}				            
					            {/*<Nav.Link><i className="fa-solid fa-clipboard-list"> Orders on my products</i></Nav.Link>  */}         
					          </Nav>
					        </Navbar.Collapse>
				     	</Container>
				    </Navbar>		    
				    {
				    	isLoading ?			    			    	
				    		<Loading/>
				    	:
				    		(orders.length > 0) ?
				    			orders
				    		:
				    		
								<Table className="mt-2" striped bordered hover>
									<thead>
										<tr>
											<td style={{textAlign: 'center'}}><i className="fa-solid fa-box-open"> No orders found!</i></td>	
										</tr>
									</thead>
								</Table>				
			    	}
			    	
			    	</>			     			    
			    :			    
			    	<Navigate to="/login"/>			    
			}
		</Container>		
	)
}