import {Container, Row, Col} from 'react-bootstrap'
import Banner from '../components/Banner'

export default function Home(){
	return (
		<Container>
	      <Row className="justify-content-md-center">
	        <Col xs lg="2"></Col>
	        <Col md="10" lg="8">
	        	<Banner/>
	        </Col>
	        <Col xs lg="2"></Col>
	      </Row>     
	    </Container>
	)
}