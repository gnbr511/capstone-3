import Loading from '../components/Loading'
import ProductsTable from '../components/ProductsTable'
import AllProducts from '../components/AllProducts'
import OrdersOnMyProductsTable from '../components/OrdersOnMyProductsTable'

import {Container, Nav, Navbar, Button, Form, Table, Row, Col, Modal} from 'react-bootstrap'

import {useState, useEffect} from 'react'
import {Navigate} from 'react-router-dom'
import Swal from 'sweetalert2'

export function Refresh (){
	document.querySelector('#refresh').click()
}

export function RefreshUnApproved (){
	document.querySelector('#refreshUnapproved').click()
}

export function RefreshOrdersOnMyProducts (){
	document.querySelector('#refreshOrdersOnMyProducts').click()
}

export function RefreshPendingOrdersOnMyProducts (){
	document.querySelector('#refreshPendingOrdersOnMyProducts').click()
}

export default function Dashboard(){
	const [searchKeyword, setSearchKeyword] = useState('')
	const [products, setProducts] = useState([])
	const [isLoading, setIsLoading] = useState(false)
	const [modalShow, setModalShow] = useState(false)
	const [diverter, setDiverter] = useState('my-products')
	const [isClickedUnapproved, setIsClickedUnapproved] = useState(false)
	const [ordersOnMyProducts, setOrdersOnMyProducts] = useState([])
	const [adminDashboardData, setAdminDashboardData] = useState([])
	const [pendingOrders, setPendingOrders] = useState([])

	const DisplayMyProducts = () => {
		// setIsLoading(true)
		fetch(`${process.env.REACT_APP_API_URL}/products/my-products`, {
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(response => response.json())
		.then(result => {			
			try{
				setProducts(
				result.map(product => <ProductsTable key={product._id} product={product}/>)
				)
			}catch(error){

			}
			setDiverter('my-products')			
			setIsLoading(false)
		})
	}

	const SearchProducts = (event) => {
		event.preventDefault()
		// setIsLoading(true)
		fetch(`${process.env.REACT_APP_API_URL}/products/my-products/search`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({searchKeyword: searchKeyword})
		})
		.then(response => response.json())
		.then(result => {			
			try{
				setProducts(
					result.map(product => <ProductsTable key={product._id} product={product}/>)
				)
			}catch(error){
				setProducts(product => 
					<tr>
						<td colSpan="9" className="text-center">
							<i className="fa-solid fa-magnifying-glass-minus mt-3"> {result.message}</i>
						</td>
					</tr>
				)
			}			
			setIsLoading(false)
					
		})
	}

	const DisplayAdminDashboardData = () => {
		// setIsLoading(true)
		fetch(`${process.env.REACT_APP_API_URL}/products/all`, {
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(response => response.json())
		.then(result => {
			try{
				setAdminDashboardData(
				result.map(product => <AllProducts key={product._id} product={product}/>)
				)
			}catch(error){

			}
			setIsLoading(false)
		})
	}

	const DisplayUnapprovedProducts = () => {
		// setIsLoading(true)
		setIsClickedUnapproved(true)
		fetch(`${process.env.REACT_APP_API_URL}/products/unapproved`, {
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(response => response.json())
		.then(result => {
			try{
				setAdminDashboardData(
				result.map(product => <AllProducts key={product._id} product={product} isClickedUnapproved={isClickedUnapproved}/>)
				)
			}catch(error){

			}
			setIsLoading(false)
		})
	}

	const AdminProductSearch = (event) => {
		event.preventDefault()
		// setIsLoading(true)
		fetch(`${process.env.REACT_APP_API_URL}/products/admin-search`, {
			method: 'POST',
			headers: {'Content-Type': 'application/json'},
			body: JSON.stringify({searchKeyword: searchKeyword})
		})
		.then(response => response.json())
		.then(result => {
			try{
				if(result.length > 0){
					setAdminDashboardData(
						result.map(product => {
							return(
								<AllProducts key={product._id} product={product}/>
							)
						})				
					)
				}else{
					setAdminDashboardData(
						<i className="fa-solid fa-magnifying-glass-minus mt-3"> No product matches the search term!</i>
					)
				}				
			}catch(error){
				adminDashboardData(
					product => <i className="fa-solid fa-magnifying-glass-minus mt-3"> {result.message}</i>
				)
			}		
			setIsLoading(false)
		})
	}

	const AddProduct = (event) => {
		event.preventDefault()
		let productName = document.querySelector('#productName').value
		let productBrand = document.querySelector('#productBrand').value
		let productDescription = document.querySelector('#productDescription').value
		let productPrice = document.querySelector('#productPrice').value
		let productStock = document.querySelector('#productStock').value
		if(productPrice <= 0){
			document.querySelector('#productPrice').focus()
		}else if(productStock <= 0){
			document.querySelector('#productStock').focus()
		}
		else{
			fetch(`${process.env.REACT_APP_API_URL}/products/create-product`,{
				method: 'POST',
				headers:{
					'Content-Type': 'application/json',
					Authorization: `Bearer ${localStorage.getItem('token')}`
				},
				body: JSON.stringify({					
					productName: productName,
					productBrand: productBrand,
					productDescription: productDescription,
					price: productPrice,
					stocksAvailable: productStock
				})			
			})
			.then(response => response.json())
			.then(result => {				
				if(result.isSuccess){
					document.querySelector('#modalCloseBtn').click()					
					Swal.fire({
						title: 'Success',
						icon: 'success',
						confirmButtonColor: "#f0ad4e",
						text: result.message
					})					
					Refresh()
				}else{
					Swal.fire({
						title: 'Error',
						icon: 'error',
						confirmButtonColor: "#f0ad4e",
						text: result.message
					})	
				}
			})
		}
	}

	const DisplayOrdersOnMyProducts = () => {
		// setIsLoading(true)
		setDiverter('orders-from-me')		
		fetch(`${process.env.REACT_APP_API_URL}/orders/my-products-orders`, {
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(response => response.json())
		.then(result => {
			try{
				setOrdersOnMyProducts(
					result.map(product => <OrdersOnMyProductsTable key={product._id} product={product} />)
				)
			}catch(error){

			}
			setDiverter('orders-from-me')
			setIsLoading(false)
		})
	}

	const DisplayPendingOrdersOnMyProducts = () => {		
		fetch(`${process.env.REACT_APP_API_URL}/orders/my-products-orders/pending`, {
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(response => response.json())
		.then(result => {
			if(result.length > 0){
				setOrdersOnMyProducts(
					result.map(product => <OrdersOnMyProductsTable key={product._id} product={product} pendingOrdersBtnClicked={true}/>)
				)
			}else{
				setOrdersOnMyProducts(
					<>
						<tr>
							<td colSpan="9" style={{textAlign: 'center'}}>
								<i className="fa-solid fa-magnifying-glass-minus mt-3"> No pending orders on your products!</i>
							</td>
						</tr>
					</>
				)
			}			
		})
	}

	function AddProductModal(props) {
	  return (
	    <Modal
	      {...props}
	      size="md"
	      aria-labelledby="contained-modal-title-vcenter"
	      centered
	    >
	      <Modal.Header closeButton  className="bg-warning">
	        <Modal.Title id="contained-modal-title-vcenter">
	         <i className="fa-solid fa-plus"> Add Product</i>
	        </Modal.Title>
	      </Modal.Header>
	      <Form onSubmit={event => AddProduct(event)}>
	      	<Modal.Body>
	      		<Form.Label style={{fontWeight: 'bold'}}>Product Name</Form.Label>	      				    
	        	<Form.Control
	              type="text"
	              id="productName"
	              placeholder="Product name"
	              className="me-2"
	              aria-label="Search"
	              required	              
	            />

	            <Form.Label className="mt-3" style={{fontWeight: 'bold'}}>Product Brand</Form.Label>	      				    
	        	<Form.Control
	              type="text"
	              id="productBrand"
	              placeholder="Product brand"
	              className="me-2"
	              aria-label="Search"
	              required	              
	            />

	            <Form.Label className="mt-2" style={{fontWeight: 'bold'}}>Product Description</Form.Label>	      				    
	        	<Form.Control
	              type="text"
	              id="productDescription"
	              placeholder="Product Description"
	              className="me-2"
	              aria-label="Search"
	              required	              
	            />

	            <Form.Label className="mt-2" style={{fontWeight: 'bold'}}>Price</Form.Label>	      				    
	        	<Form.Control
	              type="number"
	              id="productPrice"
	              placeholder="Product price"
	              className="me-2"
	              aria-label="Search"
	              required		              
	            />

	            <Form.Label className="mt-2" style={{fontWeight: 'bold'}}>Stock</Form.Label>	      				    
	        	<Form.Control
	              type="number"
	              id="productStock"
	              placeholder="Product stock"
	              className="me-2"
	              aria-label="Search"
	              required              
	            /> 
		    </Modal.Body>
		    <Modal.Footer>
		    	<Button variant="primary" type="submit">Add</Button>
		      	<Button id="modalCloseBtn" variant="danger" onClick={props.onHide}>Close</Button>
		    </Modal.Footer>
	      </Form>	      
	    </Modal>
	  );
	}

	const Refresh = () => {
		DisplayMyProducts()
		DisplayAdminDashboardData()		
		setSearchKeyword('')
	}

	useEffect(() => {		
		DisplayMyProducts()
		DisplayAdminDashboardData()		
	}, [])

	return(
		<Container>
		{
			(localStorage.getItem('id') !== null) ?
				(localStorage.getItem('isAdmin') !== "true") ?
					<>
					<Navbar collapseOnSelect expand="lg" bg="warning" variant="warning" className="mt-2 rounded">
				      <Container>
				        <Navbar.Brand><i className="fa-solid fa-chart-line"> Dashboard</i></Navbar.Brand>
				        <Navbar.Toggle aria-controls="responsive-navbar-nav" />
				        <Navbar.Collapse id="responsive-navbar-nav">
				          <Nav className="me-auto">            
				          </Nav>
				          <Nav>
				            <Nav.Link onClick={DisplayMyProducts}><i className="fa-solid fa-box-open"> My Products</i></Nav.Link>				            
				            <Nav.Link id="refreshOrdersOnMyProducts" onClick={DisplayOrdersOnMyProducts}><i className="fa-solid fa-clipboard-list"> Orders on my products</i></Nav.Link>           
				          </Nav>
				        </Navbar.Collapse>
				      </Container>
				    </Navbar>				    
				    {
				    	isLoading ?
				    		<Loading/>
				    	:				    		
				    		(diverter == 'my-products') ?
				    			<>
				    			<Row className="mt-3">
							    	<Col sm="4" md="6">
							    		<Button id="refresh" className="btn-dark" onClick={Refresh}><i className="fa-solid fa-rotate-right"> Refresh</i></Button>
							    		<span>&nbsp;</span>
							    		<Button variant="dark" onClick={() => setModalShow(true)}><i className="fa-solid fa-plus"> Add Product</i></Button>
							    		<AddProductModal
								        show={modalShow}
								        onHide={() => setModalShow(false)}
								      />
							    	</Col>
							    	<Col  sm="8" md="6">
							    		<Form className="d-flex" onSubmit={event => SearchProducts(event)}>				    
								            <Form.Control
								              type="search"
								              placeholder="Search"
								              className="me-2"
								              aria-label="Search"
								              value={searchKeyword}
								              onChange={event => setSearchKeyword(event.target.value)}		              
								            />
								            <Button variant="dark" type="submit"><i className="fa-solid fa-magnifying-glass">Search</i></Button>
							          	</Form>
							    	</Col>				    	
							    </Row>
						    	<Table className="mt-1" striped bordered hover>
							    	<thead className="bg-dark text-light">
								        <tr>
								          <th>ID</th>
								          <th>Name</th>
								          <th>Brand</th>
								          <th>Description</th>
								          <th>Price</th>
								          <th>Stock</th>
								          <th>Active</th>
								          <th>Approved</th>
								          <th colSpan="2">Actions</th>
								        </tr>
								    </thead>
								    <tbody>
								    	{products}
								    </tbody>
							    </Table>
							    </>
							:
								<>
				    			<Row className="mt-3">
							    	<Col sm="4" md="6">
							    		<Button className="btn-dark" onClick={DisplayOrdersOnMyProducts}><i className="fa-solid fa-rotate-right"> Refresh</i></Button>
							    		&nbsp;
							    		<Button id="refreshPendingOrdersOnMyProducts" className="btn-dark" onClick={DisplayPendingOrdersOnMyProducts}><i className="fa-solid fa-clock-rotate-left"> Pending Orders</i></Button>							    		
							    	</Col>
							    	<Col  sm="8" md="6">							    									    		
							    		{/*<Form className="d-flex" onSubmit={event => SearchProducts(event)}>				    
								            <Form.Control
								              type="search"
								              placeholder="Search"
								              className="me-2"								            
								              aria-label="Search"
								              value={searchKeyword}
								              onChange={event => setSearchKeyword(event.target.value)}		              
								            />
								            <Button variant="dark" type="submit"><i className="fa-solid fa-magnifying-glass">Search</i></Button>
							          	</Form>*/}
							    	</Col>				    	
							    </Row>
								<Table className="mt-1" striped bordered hover>
							    	<thead className="bg-dark text-light">
								        <tr>
								          <th>Customer</th>
								          <th>Product ID</th>
								          <th>Name</th>
								          <th>Brand</th>
								          <th>Price</th>
								          <th>Quantity</th>								          
								          <th>Subtotal</th>
								          <th>Status</th>							          
								          <th colSpan="2">Actions</th>
								        </tr>
								    </thead>
								    <tbody>
								    	{ordersOnMyProducts}
								    </tbody>
							    </Table>
							    </>
				    }				    
				    </>
				:
					<>
						<Navbar bg="warning" expand="lg" className="mt-2 rounded">
					      <Container fluid>
					        <Navbar.Brand><i className="fa-solid fa-boxes-stacked"> Products</i></Navbar.Brand>
					        <Navbar.Toggle aria-controls="navbarScroll" />
					        <Navbar.Collapse id="navbarScroll">
					          <Nav
					            className="me-auto my-2 my-lg-0"
					            style={{ maxHeight: '100px' }}
					            navbarScroll
					          >
					          <Button id="refresh" className="btn-dark" onClick={Refresh}><i className="fa-solid fa-rotate-right"></i></Button>
					          </Nav>
					          <Button id="refreshUnapproved" variant="dark" onClick={DisplayUnapprovedProducts}><i className="fa-solid fa-thumbs-down"> Unapproved</i></Button>
					           &nbsp;   
					          <Form className="d-flex" onSubmit={event => AdminProductSearch(event)}>
					            <Form.Control
					              type="search"
					              placeholder="Search"
					              className="me-2"
					              aria-label="Search"
					              value={searchKeyword}
					              onChange={event => setSearchKeyword(event.target.value)}					              
					            />
					            <Button variant="dark" type="submit"><i className="fa-solid fa-magnifying-glass">Search</i></Button>
					          </Form>
					        </Navbar.Collapse>
					      </Container>
					    </Navbar>
					    {
				    	isLoading ?
				    		<Loading/>
				    	:
				    	<Table className="mt-1" striped bordered hover>
					    	<thead className="bg-dark text-light">
						        <tr>
						          <th>Seller ID</th>
						          <th>Seller Name</th>
						          <th>Product ID</th>						          
						          <th>Name</th>
						          <th>Brand</th>
						          <th>Description</th>
						          <th>Price</th>
						          <th>Stock</th>
						          <th>Active</th>
						          <th>Approved</th>
						          <th colSpan="2">Actions</th>
						        </tr>
						    </thead>
						    <tbody>
						    	{adminDashboardData}
						    </tbody>
					    </Table>
						}
					</>
			:
				<Navigate to="/login" />
		}
		</Container>
	)
}