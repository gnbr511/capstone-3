import {useNavigate} from 'react-router-dom'
import UserContext from '../UserContext'
import {useContext, useEffect} from 'react'

export default function Logout() {
	const navigate = useNavigate()
	const {unsetUser, setUser} = useContext(UserContext)
	unsetUser()
	useEffect(() => {
		setUser({
			id: null,
			isAdmin: null
		})
	}, [])
	navigate("/login")
}