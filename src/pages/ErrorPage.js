import {Container, Row, Col} from 'react-bootstrap'
import BannerCarousel from '../components/BannerCarousel'
import {Link} from 'react-router-dom'

export default function ErrorPage(){
	return(
		<Container>
	      <Row className="justify-content-md-center mt-5">
	      	<Col xs md="4" lg="6" className=""><BannerCarousel/></Col>
	        <Col md="8" lg="6" className="mt-md-5 mt-lg-5">
	        	<h1>Oops!</h1>
	        	<p>Error 404. Page not found!</p>
	        	<Link to="/">Go back to home</Link>
	        </Col>	   
	      </Row>
		</Container>
	)	
}

