import {Form, Button, Row, Col, Container, Card} from 'react-bootstrap'
import {useState, useEffect} from 'react'
import {Navigate, useNavigate, Link} from 'react-router-dom'
import BannerCarousel from '../components/BannerCarousel'
import Swal from 'sweetalert2'

export default function Register(){
	const navigate = useNavigate()
	const [firstName, setFirstName] = useState('')
	const [lastName, setLastName] = useState('')
	const [mobileNo, setMobileNo] = useState('')
	const [address, setAddress] = useState('')
	const [birthdate, setBirthdate] = useState('')
	const [email, setEmail] = useState('')
	const [password1, setPassword1] = useState('')
	const [password2, setPassword2] = useState('')
	const [isActive, setIsActive] = useState('')

	useEffect(() => {
		((firstName !== '' && lastName !== '' && mobileNo.length === 11 && address !== '' && birthdate !== '' && email !== '' && password1 !== '' && password2 !== '') && (password1 === password2)) ?
		setIsActive(true):setIsActive(false)
	}, [firstName, lastName, mobileNo, address, birthdate, email, password1, password2])
	function clearFields(message){
		setEmail('')
		setPassword1('')
		setPassword2('')
		setFirstName('')
		setLastName('')
		setMobileNo('')
		setAddress('')
		setBirthdate('')
		Swal.fire({
			title: 'Success',
			icon: 'success',
			confirmButtonColor: "#f0ad4e",
			text: message
		})
		navigate("/login")
	}

	function registerUser(event){
		event.preventDefault()
		fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				firstName: firstName,
				lastName: lastName,
				mobileNo: mobileNo,
				address: address,
				birthDate: birthdate,
				email: email,
				password: password1
			})
		})
		.then(response => response.json())
		.then(result => {
			result.isSuccess ?				
				clearFields(result.message)
			:
				Swal.fire({
					title: 'Error',
					icon: 'error',
					confirmButtonColor: "#f0ad4e",
					text: result.message
				})
		})
	}

	return(
		(localStorage.getItem('id') === null) ?
		<Container>
	      <Row className="justify-content-md-center mt-3">
	      	<Col xs lg="8" className="mt-md-2 mt-lg-5"><BannerCarousel/></Col>
	        <Col lg="4" className="mt-0">
	        	<Card border="dark" text="dark">
			      <Card.Header className="bg-warning"><h3><i className="fa-solid fa-pen-to-square"></i>Register</h3></Card.Header>
			      <Card.Body>
			      	<Form onSubmit={event => registerUser(event)}>
						<Form.Group controlId="firstName">
			                <Form.Label>First name</Form.Label>
			                <Form.Control 
				                type="text" 
				                placeholder="Enter first name"
				                value={firstName}
				                onChange={event => setFirstName(event.target.value)}
				                required
			                />	              
			            </Form.Group>
			            <Form.Group controlId="lastName">
			                <Form.Label>Last name</Form.Label>
			                <Form.Control 
				                type="text" 
				                placeholder="Enter last name"
				                value={lastName}
				                onChange={event => setLastName(event.target.value)}
				                required
			                />	              
			            </Form.Group>
			            <Form.Group controlId="mobileNo">
			                <Form.Label>Mobile no</Form.Label>
			                <Form.Control 
				                type="text" 
				                placeholder="Enter mobile no"
				                value={mobileNo}
				                onChange={event => setMobileNo(event.target.value)}
				                required
			                />	              
			            </Form.Group>
			            <Form.Group controlId="address">
			                <Form.Label>Address</Form.Label>
			                <Form.Control 
				                type="text" 
				                placeholder="Enter your address"
				                value={address}
				                onChange={event => setAddress(event.target.value)}
				                required
			                />	              
			            </Form.Group>
			            <Form.Group controlId="birthdate">
			                <Form.Label>Birthdate</Form.Label>
			                <Form.Control 
				                type="date" 
				                placeholder="Enter your birthdate"
				                value={birthdate}
				                onChange={event => setBirthdate(event.target.value)}
				                required
			                />	              
			            </Form.Group>
			            <Form.Group controlId="userEmail">
			                <Form.Label>Email address</Form.Label>
			                <Form.Control 
				                type="email" 
				                placeholder="Enter email"
				                value={email}
				                onChange={event => setEmail(event.target.value)}
				                required
			                />
			                <Form.Text className="text-muted">
			                    We'll never share your email with anyone else.
			                </Form.Text>
			            </Form.Group>
			            <Row>
			            	<Col xs="6">
					            <Form.Group controlId="password1">
					                <Form.Label>Password</Form.Label>
					                <Form.Control 
						                type="password" 
						                placeholder="Password" 
						                value={password1}
						                onChange={event => setPassword1(event.target.value)}
						                required
					                />
					            </Form.Group>
			            	</Col>
			            	<Col xs="6">
		            			 <Form.Group controlId="password2">
					                <Form.Label>Verify Password</Form.Label>
					                <Form.Control 
						                type="password" 
						                placeholder="Verify Password"
						                value={password2}
						                onChange={event => setPassword2(event.target.value)}
						                required
					                />
					            </Form.Group>
			            	</Col>
			            </Row>			           
			            {	isActive ? 
			            	<Button variant="warning border-dark" type="submit" id="submitBtn" className="mt-3">
			            		Submit <i className="fa-solid fa-arrow-right"></i>
			            	</Button>
			            	:
			            	<Button variant="warning border-dark" type="submit" id="submitBtn" disabled className="mt-3">
			            		Submit <i className="fa-solid fa-arrow-right"></i>
			            	</Button>
			            }
			        </Form>
			      </Card.Body>
			    </Card>
			    <Link to="/login">Already have an account? Log in</Link>		    		            	
	        </Col>	   
	      </Row>     
	    </Container>
	    :
	    <Navigate to="/dashboard"/>		
	)
}