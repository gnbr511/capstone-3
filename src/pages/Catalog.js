import {Row, Navbar, Nav, Container, Button, Form, Col} from 'react-bootstrap'
import {Link} from 'react-router-dom'
import ProductCard from '../components/ProductCard'
import {useEffect, useState} from 'react'
import Loading from '../components/Loading'

export default function Catalog() {
	const [products, setProducts] = useState([])
	const [searchKeyword, setSearchKeyword] = useState('')
	const [isLoading, setIsLoading] = useState(false)
	
	const GetAllProducts = () => {
		setIsLoading(true)
		fetch(`${process.env.REACT_APP_API_URL}/products/`)
		.then(response => response.json())
		.then(result => {
			setProducts(
				result.map(product => {
					return(
						<ProductCard key={product._id} product={product}/>
					)
				})
			)
			setIsLoading(false)
		})
	}

	const Search = (event) => {
		event.preventDefault()
		setIsLoading(true)
		fetch(`${process.env.REACT_APP_API_URL}/products/search`, {
			method: 'POST',
			headers: {'Content-Type': 'application/json'},
			body: JSON.stringify({searchKeyword: searchKeyword})
		})
		.then(response => response.json())
		.then(result => {
			try{
				setProducts(
					result.map(product => {
						return(
							<ProductCard key={product._id} product={product}/>
						)
					})				
				)
			}catch(error){
				setProducts(
					product => <i className="fa-solid fa-magnifying-glass-minus mt-3"> {result.message}</i>
				)
			}		
			setIsLoading(false)
		})
	}

	const Refresh = () => {
		GetAllProducts()
		setSearchKeyword('')
	}

	useEffect(() => {
		GetAllProducts()
	}, [])

	return(		
		<Container>
			<Navbar bg="warning" expand="lg" className="mt-2 rounded">
		      <Container fluid>
		        <Navbar.Brand><i className="fa-solid fa-boxes-stacked"> Products</i></Navbar.Brand>
		        <Navbar.Toggle aria-controls="navbarScroll" />
		        <Navbar.Collapse id="navbarScroll">
		          <Nav
		            className="me-auto my-2 my-lg-0"
		            style={{ maxHeight: '100px' }}
		            navbarScroll
		          >
		          <Button className="btn-dark" onClick={event=> Refresh()}><i className="fa-solid fa-rotate-right"></i></Button>
		          </Nav>	          
		          <Form className="d-flex" onSubmit={event => Search(event)}>
		            <Form.Control
		              type="search"
		              placeholder="Search"
		              className="me-2"
		              aria-label="Search"
		              value={searchKeyword}
		              onChange={event => setSearchKeyword(event.target.value)}
		            />
		            <Button variant="dark" type="submit"><i className="fa-solid fa-magnifying-glass">Search</i></Button>
		          </Form>
		        </Navbar.Collapse>
		      </Container>
		    </Navbar>
			<Row>
				{
					(!isLoading) ?
						<>
						{products}
						</>
					:
						<Container className="mt-2">
							<Loading/>
						</Container>						
				}
			</Row>
		</Container>			
	)	
}