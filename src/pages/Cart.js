import {Navbar, Nav, Button, Table, Container, Row, Col} from 'react-bootstrap'
import Loading from '../components/Loading'
import CartItems from '../components/CartItems'
import {useState, useEffect} from 'react'
import {useNavigate, Navigate} from 'react-router-dom'
import Swal from 'sweetalert2'

export function Refresh (){
	document.querySelector('#refresh').click()
}

export default function Cart(){
	const [isLoading, setIsLoading] = useState(false)
	const [cartItems, setCartItems] = useState([])
	const [totalAmount, setTotalAmount] = useState(0)
	const navigate = useNavigate()

	const Formatter = new Intl.NumberFormat('en-US', {
		style: 'currency',
		currency: 'Php',
	})

	const GetCartItems = () => {
		// setIsLoading(true)
		fetch(`${process.env.REACT_APP_API_URL}/carts/my-cart`, {
			method: 'GET',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(response => response.json())
		.then(result => {
			try{
				setCartItems(
					result[0].products.map(item => <CartItems key={item._id} item={item} />)
				)				
				setTotalAmount(result[0].totalAmount)
			}catch(error){

			}
			setIsLoading(false)
		})		
	}

	const CheckOut = () => {
		Swal.fire({
		  title: 'Click ok to confirm.',		  
		  showCancelButton: true,
		  confirmButtonColor: "#f0ad4e",
		  confirmButtonText: 'Confirm'
		}).then((result) => {		  
			  if (result.isConfirmed) {
			    // setIsLoading(true)
				fetch(`${process.env.REACT_APP_API_URL}/carts/check-out`, {
					method: 'POST',
					headers: {
						'Content-Type': 'application/json',
						 Authorization: `Bearer ${localStorage.getItem('token')}`
					},
					body: JSON.stringify({
						paymentMethod: "cash-on-delivery",
						amountPaid: totalAmount
					})
				})
				.then(response => response.json())
				.then(result => {
					if(result.isSuccess){								
						Swal.fire({						
							title: 'Success',
							icon: 'success',
							confirmButtonColor: "#f0ad4e",
							text: result.message
						})					
						navigate('/orders')
					}else{					
						Swal.fire({
							title: 'Error',
							icon: 'error',
							confirmButtonColor: "#f0ad4e",
							text: result.message
						})								
					}
					isLoading(false)
				})
			  } else if (result.isDenied) {
			    Swal.fire('Changes are not saved', '', 'info')
			  }
		})		
	}

	useEffect(() => {
		GetCartItems()
	}, [])

	return(
		<>
		{
			(localStorage.getItem('id') !== null && localStorage.getItem('isAdmin') !== 'true') ?
			<Container>
			<Navbar collapseOnSelect expand="lg" bg="warning" variant="warning" className="mt-2 rounded">
				<Container>
			        <Navbar.Brand><i className="fa-solid fa-cart-shopping"> Cart</i></Navbar.Brand>
			        <Navbar.Toggle aria-controls="responsive-navbar-nav" />
			        <Navbar.Collapse id="responsive-navbar-nav">
			          <Nav className="me-auto">
			          	<Button className="btn-dark" id="refresh" onClick={event=> GetCartItems()}><i className="fa-solid fa-rotate-right"></i></Button>      
			          </Nav>
			          <Nav>
			            {/*<Nav.Link><i className="fa-solid fa-box-open"> My Products</i></Nav.Link>*/}				            
			            {/*<Nav.Link><i className="fa-solid fa-clipboard-list"> Orders on my products</i></Nav.Link>  */}         
			          </Nav>
			        </Navbar.Collapse>
		     	</Container>
		    </Navbar>	    
		    {
		    	isLoading ?
		    		<Loading />
		    	:
		    		<Table className="mt-1" striped bordered hover>
				    	<thead className="bg-dark text-light">
					        <tr>			         
					          <th>Name</th>
					          <th>Brand</th>
					          <th>Price</th>
					          <th>Quantity</th>
					          <th>Subtotal</th>
					          <th>Actions</th>
					        </tr>
					    </thead>
					    <tbody>
					    	{cartItems}
					    	{
					    		(totalAmount > 0) ?
							    	<tr>
								    	<td colSpan="3"></td>
								    	<td style={{textAlign: 'right', fontWeight: 'bold'}}>Total Amount</td>
								    	<td style={{fontWeight: 'bold'}}>{Formatter.format(totalAmount)}</td>
								    	<td colSpan="2" style={{textAlign: 'center'}}><Button variant="warning" onClick={CheckOut}><i className="fa-solid fa-bag-shopping"> Check out</i></Button></td>
							    	</tr>
							    :
							    	<tr>
										<td colSpan="7" className="text-center">
											<i className="fa-solid fa-inbox mt-3"> No items in your cart!</i>
										</td>
									</tr>
					    	}				    	
					    </tbody>

				    </Table>
		    }
		    </Container>
		    :
		    <Navigate to="/login"/>

		}		
	    </>
	)	
}