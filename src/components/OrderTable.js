import Table from 'react-bootstrap/Table'
import PropTypes from 'prop-types'
import {useState, useEffect} from 'react'

export default function OrderTable(order) {
	const {_id, products, totalAmount, paymentMethod, amountPaid, isPaid, purchasedOn} = order.order
	const [tablesOfItems, setTablesOfItems] = useState([])

	const Formatter = new Intl.NumberFormat('en-US', {
		style: 'currency',
		currency: 'Php',
	})

	useEffect(() => {
		setTablesOfItems(
			products.map(item => {
				return(					
					<tr key={item._id}>
						<td>{item.productName}</td>
						<td>{item.productBrand}</td>
						<td>{Formatter.format(item.price)}</td>
						<td>{item.quantity}</td>
						<td>{Formatter.format(item.subtotal)}</td>
						<td>{item.status}</td>
					</tr>
				)
			})
		)
	}, [])

	return(
		<Table className="mt-2 mb-5" striped bordered hover>
			<thead className="">
				<tr className="bg-dark text-light">
					<th colSpan="6">Order ID: {_id}</th>
				</tr>
				<tr>
					<th colSpan="6" style={{textAlign: 'center'}}>Ordered Items</th>
				</tr>				
			</thead>
			<tbody>
				<tr>
					<th>Name</th>
					<th>Brand</th>
					<th>Price</th>
					<th>Quantity</th>
					<th>Subtotal</th>
					<th>Status</th>
				</tr>			
				{tablesOfItems}
				<tr>
					<td colSpan="3">
						<span style={{fontWeight: 'bold'}}>Payment Method:</span> {paymentMethod}
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<span style={{fontWeight: 'bold'}}>Order Date:</span> {purchasedOn.toString('YYYY-MM-DD')}
					</td>					
					<th style={{textAlign: 'center'}}>Total Amount</th>
					<th colSpan="2">{Formatter.format(totalAmount)}</th>
				</tr>
			</tbody>
		</Table>
	)

}

OrderTable.propTypes = {
	order: PropTypes.shape({
		_id: PropTypes.string.isRequired,
		products: PropTypes.arrayOf(PropTypes.shape({
			sellerId: PropTypes.string.isRequired,
            productId: PropTypes.string.isRequired,
            productName: PropTypes.string.isRequired,
            productBrand: PropTypes.string.isRequired,
            price: PropTypes.number.isRequired,
            quantity: PropTypes.number.isRequired,
            subtotal: PropTypes.number.isRequired,
            status: PropTypes.string.isRequired,
            _id: PropTypes.string.isRequired
		})),
		totalAmount: PropTypes.number.isRequired,
        paymentMethod: PropTypes.string.isRequired,
        amountPaid: PropTypes.number.isRequired,
        isPaid: PropTypes.bool.isRequired
       
	})
}