import {Table, Button, Form} from 'react-bootstrap'
import PropTypes from 'prop-types'
import Swal from 'sweetalert2'
import {RefreshOrdersOnMyProducts, RefreshPendingOrdersOnMyProducts} from '../pages/Dashboard'
import {useState, useEffect} from 'react'

export default function OrdersOnMyProductsTable(product){
	const [customerName, setCustomerName] = useState('')
	const {_id, customerId, sellerId, productId, productName, productBrand, price, quantity, subtotal, status} = product.product

	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/users/${customerId}`)
		.then(response => response.json())
		.then(result => {
			setCustomerName(`${result.firstName} ${result.lastName}`)
		})
	}, [])

	const AcceptOrder = (id) => {
		fetch(`${process.env.REACT_APP_API_URL}/orders/${id}/accept`,{
			method: 'PATCH',
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(response => response.json())
		.then(result => {
			if(result.isSuccess){
				RefreshPendingOrdersOnMyProducts()			
				Swal.fire({
					title: 'Success',
					icon: 'success',
					confirmButtonColor: "#f0ad4e",
					text: result.message
				})
			}else{
				RefreshPendingOrdersOnMyProducts()
				Swal.fire({
					title: 'error',
					icon: 'error',
					confirmButtonColor: "#f0ad4e",
					text: result.message
				})
			}
		})
	}

	const ShipOrder = (id) => {
		fetch(`${process.env.REACT_APP_API_URL}/orders/${id}/ship`, {
			method: 'PATCH',
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(response => response.json())
		.then(result => {
			if(result.isSuccess){
				RefreshOrdersOnMyProducts()			
				Swal.fire({
					title: 'Success',
					icon: 'success',
					confirmButtonColor: "#f0ad4e",
					text: result.message
				})
			}else{
				RefreshOrdersOnMyProducts()
				Swal.fire({
					title: 'error',
					icon: 'error',
					confirmButtonColor: "#f0ad4e",
					text: result.message
				})
			}
		})
	}

	const DeliverOrder = (id) => {
		fetch(`${process.env.REACT_APP_API_URL}/orders/${id}/deliver`, {
			method: 'PATCH',
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(response => response.json())
		.then(result => {
			if(result.isSuccess){
				RefreshOrdersOnMyProducts()			
				Swal.fire({
					title: 'Success',
					icon: 'success',
					confirmButtonColor: "#f0ad4e",
					text: result.message
				})
			}else{
				RefreshOrdersOnMyProducts()
				Swal.fire({
					title: 'error',
					icon: 'error',
					confirmButtonColor: "#f0ad4e",
					text: result.message
				})
			}
		})
	}

	return(
		<tr>
			<td>{customerName}</td>
			<td>{productId}</td>			
			<td>{productName}</td>			
			<td>{productBrand}</td>			
			<td>{price}</td>			
			<td>{quantity}</td>			
			<td>{subtotal}</td>			
			<td>{status}</td>
			<td style={{textAlign: 'center'}}>
				{
					(status !== 'pending') ?
						(status !== 'accepted') ?
							(status !== 'shipped') ?
							<>
								<Button onClick={() => ShipOrder(_id)} disabled={status !== ''}>Delivered</Button>
							</>
							:
							<>
								<Button onClick={() => DeliverOrder(_id)} disabled={status === 'delivered'}>Delivered</Button>
							</>
						:
						<>
							<Button onClick={() => ShipOrder(_id)} disabled={status === 'shipped'}>Order Shipped</Button>
						</>
					:
					<>
						<Button onClick={() => AcceptOrder(_id)} disabled={status === 'accepted'}>Accept</Button>
					</>


				}									
			</td>			
		</tr>
	)
}