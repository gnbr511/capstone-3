import {Row, Col, Container, Button, Carousel, Card, Form, InputGroup} from 'react-bootstrap'
import {Link, useParams, useNavigate} from 'react-router-dom'
import {useState, useEffect, useContext} from 'react'
import Loading from './Loading'
import Swal from 'sweetalert2'

export default function ProductView(){
	const {productId} = useParams()
	const [name, setName] = useState('') 
	const [description, setDescription] = useState('') 
	const [brand, setBrand] = useState('') 
	const [price, setPrice] = useState(0) 
	const [stock, setStock] = useState(0)
	const [quantity, setQuantity] = useState(1)
	const [sellerName, setSellerName] = useState('')
	const [sellerId, setSellerId] = useState('')
	const [isLoading, setIsLoading] = useState(false)
	const [isSubmitted, setIsSubmitted] = useState(false)
	const navigate = useNavigate()

	const Formatter = new Intl.NumberFormat('en-US', {
		style: 'currency',
		currency: 'Php',
	})

	let newQty = 0
	function Increment(){
		newQty = quantity + 1	
		setQuantity(newQty)
	}

	function Decrement(){
		if(quantity	> 1){
			newQty = quantity - 1	
			setQuantity(newQty)
		}		
	}

	const AddToCart = (event) => {
		event.preventDefault()
		if(!isSubmitted){
			setIsSubmitted(true)
			fetch(`${process.env.REACT_APP_API_URL}/carts/${productId}/add-to-cart`, {
				method: 'POST',
				headers: {
					'Content-Type': 'application/json',
					Authorization: `Bearer ${localStorage.getItem('token')}`
				},
				body: JSON.stringify({quantity: quantity})
			})
			.then(response => response.json())
			.then(result => {
				if(result.isSuccess){
					Swal.fire({
						title: 'Success',
						icon: 'success',
						confirmButtonColor: "#f0ad4e",
						text: result.message					
					})
					navigate('/cart')
				}else{
					Swal.fire({
						title: 'Error',
						icon: 'error',
						confirmButtonColor: "#f0ad4e",
						text: result.message
					})
				}
			})
		}
	}

		useEffect(() => {
			setIsLoading(true)
			fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
			.then(response => response.json())
			.then(result => {
				setSellerId(result.sellerId)
				setName(result.productName)
				setDescription(result.productDescription)
				setBrand(result.productBrand)
				setPrice(result.price)
				setStock(result.stocksAvailable)
				setSellerName(`${result.sellerFirstName} ${result.sellerLastName}`)
				setIsLoading(false)		
			})

		}, [])

	return(
		<Container className="mt-3">
			<Row>
				<Col>
					<Link className="btn btn-warning border-dark" to="/products"><i className="fa-solid fa-circle-arrow-left"> Go Back</i></Link>
				</Col>
			</Row>
			{
				isLoading ?
					<>
					<Loading/>
					</>
				:
					<>					
					<Row className="mt-3">
						<Col xs="12" md="6">
							<Carousel>					 
							  <Carousel.Item interval="3000">					  		
							      	<div className="carouselOpacity">
								        <img
								          className="d-block w-100 rounded"
								          src="#"
								          alt="Product Photo"
								        />
							        </div>
							  </Carousel.Item>
							  <Carousel.Item interval="3000">					  	  
							      <div className="carouselOpacity">
								        <img
								          className="d-block w-100 rounded"
								          src="#"
								          alt="Product Photo"
								        />
							       </div>
							  </Carousel.Item>
							  <Carousel.Item interval="3000">					  	
							  	<div className="carouselOpacity">
							        <img
							          className="d-block w-100 rounded"
							          src="#"
							          alt="Product Photo"
							        />
							    </div>
							  </Carousel.Item>
							</Carousel>
						</Col>
						<Col xs="12" md="6">
							<Card>
								<Card.Header className="bg-warning">
									<Card.Title>{name}</Card.Title>
								</Card.Header>
								<Card.Body>							
									<Card.Subtitle>Brand </Card.Subtitle>
									<Card.Text>{brand}</Card.Text>
									<Card.Subtitle>Description</Card.Subtitle>
									<Card.Text>{description}</Card.Text>
									<Card.Subtitle>Price</Card.Subtitle>
									<Card.Text>{Formatter.format(price)}</Card.Text>
									<Card.Subtitle>Stock</Card.Subtitle>
									<Card.Text>{stock}</Card.Text>
									<Card.Subtitle>Seller</Card.Subtitle>
									<Card.Text>{sellerName}</Card.Text>
								</Card.Body>
							</Card>
							<Form className="mt-2" onSubmit={event => AddToCart(event)}>								
							    {
							    	(localStorage.getItem('isAdmin') !== "true") ?
							    		(localStorage.getItem('id') !== null) ?
							    			(localStorage.getItem('id') !== sellerId) ?
							    				<>
							    				<Row>
							    				<Col className="col-6 col-lg-4">
							    				<InputGroup className="mb-3">
											        <Button variant="outline-dark" id="button-addon1" onClick={Increment}>
											        	<i className="fa-solid fa-plus"></i>
											        </Button>
											        <Form.Control
											          style={{ 'textAlign': 'center' }}
											          type="number"
											          aria-label="Example text with button addon"
											          aria-describedby="basic-addon1"
											          value={quantity}
											          onChange={event => setQuantity(event.target.value)}
											        />
											        <Button variant="outline-dark" id="button-minuson1" onClick={Decrement}>
											        	<i className="fa-solid fa-minus"></i>
											        </Button>
											    </InputGroup>
											    </Col>
											    <Col className="col-3 col-lg-4">
											    </Col>
											    <Col className="col-3 col-lg-4">
											    </Col>
											    </Row>
							    				<Button type="submit" className="bg-warning border-dark text-dark"><i className="fa-solid fa-cart-plus"> Add to Cart</i></Button>
							    				</>
							    			:
							    				<Form.Label><i className="fa-solid fa-user"> This is your product.</i></Form.Label>
							    		:
							    			<Link to="/login" className="btn btn-warning border-dark">Login to Purchase</Link>
							    	:
							    		<Form.Label><i className="fa-solid fa-user"> You are an admin.</i></Form.Label>
							    }			
						    </Form>

						</Col>
					</Row>				
					</>
			}
		</Container>	
	)	
}