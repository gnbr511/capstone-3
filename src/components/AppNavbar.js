import {Navbar, Nav, Container, NavDropdown} from 'react-bootstrap'
import {Link, NavLink} from 'react-router-dom'
import {useContext} from 'react'
import UserContext from '../UserContext'

export default function AppNavbar(){
	const {user} = useContext(UserContext)
	return(
	    <Navbar collapseOnSelect expand="lg" bg="dark" variant="dark">
	      <Container>
	        <Navbar.Brand as={NavLink} to="/"><i className="fa-solid fa-cart-shopping"> GOBAS</i></Navbar.Brand>
	        <Navbar.Toggle aria-controls="responsive-navbar-nav" />
	        <Navbar.Collapse id="responsive-navbar-nav">
	          <Nav className="me-auto">
	          	<Nav.Link as={NavLink} to="/"><i className="fa-solid fa-house"></i> Home</Nav.Link>
	            <Nav.Link as={NavLink} to="/products"><i className="fa-solid fa-list-ul"></i> Catalog</Nav.Link>	           
	          </Nav>
	          <Nav>       	
	          	{
	          		(localStorage.getItem('id') !== null) ?
	          			<>
	          			<Nav.Link as={NavLink} to="/dashboard"><i className="fa-solid fa-chart-line"></i> Dashboard</Nav.Link>
	          			{
	          				(localStorage.getItem('isAdmin') === "true") ?
	          					<NavDropdown title="More" id="collasible-nav-dropdown" bg="dark" variant="warning" className="bg-dark">
					              <NavDropdown.Item href="#action/3.1">
					              	<Nav.Link href="#deets4" className="text-dark"><i className="fa-solid fa-address-card"></i> Profile</Nav.Link>
					              </NavDropdown.Item>
					              <NavDropdown.Item href="#action/3.2">
					               <Nav.Link as={NavLink} to="/logout" className="text-dark"><i className="fa-solid fa-right-from-bracket"></i> Log out</Nav.Link>
					              </NavDropdown.Item>
					            </NavDropdown>
					        :
					        	<>
					        	<Nav.Link as={NavLink} to="/cart"><i className="fa-solid fa-cart-arrow-down"></i> Cart</Nav.Link>
					          	<Nav.Link as={NavLink} to="/orders"><i className="fa-solid fa-clipboard-list"></i> Orders</Nav.Link>

			          			<NavDropdown title="More" id="collasible-nav-dropdown" bg="dark" variant="warning" className="bg-dark">			          			
					              <NavDropdown.Item href="#action/3.1">
					              	<Nav.Link href="#deets4" className="text-dark"><i className="fa-solid fa-address-card"></i> Profile</Nav.Link>
					              </NavDropdown.Item>
					              <NavDropdown.Item href="#action/3.2">
					               <Nav.Link as={NavLink} to="/logout" className="text-dark"><i className="fa-solid fa-right-from-bracket"></i> Log out</Nav.Link>
					              </NavDropdown.Item>
					            </NavDropdown>
					            </>
	          			}			          	
			            </>
	          		:
	          			<>
	          			<Nav.Link as={NavLink} to="/login"><i className="fa-solid fa-right-to-bracket"></i> Log in</Nav.Link>
	            		<Nav.Link as={NavLink} to="/register"><i className="fa-solid fa-pen-to-square"></i> Register</Nav.Link>
	            		</>
	          	} 
	          </Nav>
	        </Navbar.Collapse>
	      </Container>
	    </Navbar>
	)
}