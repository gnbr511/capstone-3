import {Row, Col, Card, Button} from 'react-bootstrap'
import PropTypes from 'prop-types'
import {useState, useEffect} from 'react'
import {Link} from 'react-router-dom'

export default function ProductCard({product}) {
	const {_id, productName, productBrand, productDescription, price, stocksAvailable} = product

	const Formatter = new Intl.NumberFormat('en-US', {
		style: 'currency',
		currency: 'Php',
	})

	return(		
		<Col xs={12} md={6} lg={3} className="mt-3 rounded">
			<Card className="productCard">
		      <Card.Img variant="top" src="asdf" />
		      <Card.Body>
		        <Card.Title>{productName}</Card.Title>
		        <Card.Subtitle>{productBrand}</Card.Subtitle>
		        <Card.Text>{productDescription}</Card.Text>
		        <Card.Subtitle>{Formatter.format(price)}</Card.Subtitle>
		        <Card.Text>stock: {stocksAvailable}</Card.Text>
		      </Card.Body>
		      <Card.Header className="bg-dark">			      	
		      	<Link className="btn btn-warning border-light" to={`/products/${_id}`}><i className="fa-solid fa-circle-info"></i>&nbsp;Details</Link>
		      </Card.Header>
		    </Card>
		</Col>
	)
}

ProductCard.propTypes = {
	product: PropTypes.shape({
		productName: PropTypes.string.isRequired,
		productBrand: PropTypes.string.isRequired,
		productDescription: PropTypes.string.isRequired,
		price: PropTypes.number.isRequired,
		stocksAvailable: PropTypes.number.isRequired
	})
}
