import {Table, Button, Modal, Form} from 'react-bootstrap'
import PropTypes from 'prop-types'
import {Refresh} from '../pages/Cart'
import Swal from 'sweetalert2'

export default function CartItems(item){
	const {sellerId, productId, productName, productBrand, price, quantity, subtotal, _id} = item.item

	const Formatter = new Intl.NumberFormat('en-US', {
		style: 'currency',
		currency: 'Php',
	})

	const RemoveItem = (id) => {
		fetch(`${process.env.REACT_APP_API_URL}/carts/product/${id}/remove`, {
			method: 'PATCH',
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(response => response.json())
		.then(result => {
			if(result.isSuccess){
				Swal.fire({
					title: 'Success',
					icon: 'success',
					confirmButtonColor: "#f0ad4e",
					text: result.message
				})
				Refresh()
			}else{
				Swal.fire({
					title: 'Error',
					icon: 'error',
					confirmButtonColor: "#f0ad4e",
					text: result.message
				})
				Refresh()
			}
		})
	}

	const ChangeQuantity = (id, no) => {		
		fetch(`${process.env.REACT_APP_API_URL}/carts/product/${id}/change-qty`, {
			method: 'PATCH',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				quantity: no
			})
		})
		.then(response => response.json())
		.then(result => {
			if(result.isSuccess){
				Refresh()
			}else{
				Refresh()
			}
		})
	}

	return(
		<tr>
			<td>{productName}</td>
			<td>{productBrand}</td>
			<td>{Formatter.format(price)}</td>
			<td>{quantity}</td>
			<td>{Formatter.format(subtotal)}</td>
			<td style={{textAlign: 'center'}}>
				<Button onClick={() => ChangeQuantity(_id, 1)} variant="primary">
 					<i className="fa-solid fa-plus"></i>
 				</Button>
 				&nbsp; 				
				<Button onClick={() => {ChangeQuantity(_id, -1)}} disabled={quantity < 2} variant="primary">
				<i className="fa-solid fa-minus"></i>
				</Button> 							
 				&nbsp;
 				<Button variant="danger" onClick={() => RemoveItem(_id)}>
 				<i className="fa-solid fa-trash-can"></i>
 				</Button>
 			</td>
		</tr>
	)
}

CartItems.propTypes = {
	item: PropTypes.shape({
		_id: PropTypes.string.isRequired,
		sellerId: PropTypes.string.isRequired,
		productBrand: PropTypes.string.isRequired,
		sellerId: PropTypes.string.isRequired,
		productName: PropTypes.string.isRequired,
		productBrand: PropTypes.string.isRequired,		
		price: PropTypes.number.isRequired,
		quantity: PropTypes.number.isRequired,	
	})
}