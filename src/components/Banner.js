import {Button, Row, Col, Container, Carousel} from 'react-bootstrap'
import {Link} from 'react-router-dom'
import BannerCarousel from './BannerCarousel'
export default function Banner(){
	return (
		<>
			<Row className = "p-5">
				<Col>
					<h1><i className="fa-solid fa-cart-shopping"> G O B A S</i></h1>				
				</Col>
				<Col>
					<i className="fa-solid">
						<h3>People's Online</h3>
						<h3>Buy And Sell</h3>
						<h3>Platform</h3>
					</i>
					<Link to="/register" className="btn btn-warning border-dark"><i className="fa-solid fa-play"> Get started</i></Link>
				</Col>
			</Row>
			<Row>
				<BannerCarousel/>
			</Row>
		</>
	)
}