import {Table, Button, Form} from 'react-bootstrap'
import PropTypes from 'prop-types'
import Swal from 'sweetalert2'
import {Refresh, RefreshUnApproved} from '../pages/Dashboard'
import {useState, useEffect} from 'react'

export default function ProductsTable({product, isClickedUnapproved}){
	const {_id, userId,  productName, productBrand, productDescription, price, stocksAvailable, isActive, isApprovedByAdmin} = product	

	const [sellerName, setSellerName] = useState('')

	const Formatter = new Intl.NumberFormat('en-US', {
		style: 'currency',
		currency: 'Php',
	})

	const DisableProduct = (id) => {
		fetch(`${process.env.REACT_APP_API_URL}/products/${id}/ban`, {
			method: 'PATCH',
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(response => response.json())
		.then(result => {			
			if(result.isSuccess){
				Refresh()
				Swal.fire({
					title: 'Success',
					icon: 'success',
					confirmButtonColor: "#f0ad4e",
					text: result.message
				})					
			}else{
				Refresh()
				Swal.fire({
					title: 'Error',
					icon: 'error',
					confirmButtonColor: "#f0ad4e",
					text: result.message
				})
			}
			
		})
	}

	const ApproveProduct = (id) => {
		fetch(`${process.env.REACT_APP_API_URL}/products/${id}/approve`, {
			method: 'PATCH',
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(response => response.json())
		.then(result => {			
			if(result.isSuccess){
				isClickedUnapproved ?
					RefreshUnApproved()
				:				
					Refresh()
				Swal.fire({
					title: 'Success',
					icon: 'success',
					confirmButtonColor: "#f0ad4e",
					text: result.message
				})				
			}else{
				Refresh()
				Swal.fire({
					title: 'Error',
					icon: 'error',
					confirmButtonColor: "#f0ad4e",
					text: result.message
				})				
			}			
		})
	}

	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/products/${_id}`)
		.then(response => response.json())
		.then(result => {			
			setSellerName(`${result.sellerFirstName} ${result.sellerLastName}`)		
		})
	})

	return(
		<tr>
			<td>{userId}</td>
			<td>{sellerName}</td>
			<td>{_id}</td>			
			<td>{productName}</td>
			<td>{productBrand}</td>
			<td>{productDescription}</td>
			<td>{Formatter.format(price)}</td>
			<td>{stocksAvailable}</td>
			<td>{isActive ? 'Yes' : 'No'}</td>
			<td>{isApprovedByAdmin ? 'Yes' : 'No'}</td>			
			<td>
				{isApprovedByAdmin ?
					<Button variant="danger" onClick={() => DisableProduct(_id)} className="border-dark text-light">Disable</Button>
				:
					<Button variant="success" onClick={() => ApproveProduct(_id)} className="border-dark text-light">Approve</Button>
				}			
			</td>
		</tr>
	)
}

ProductsTable.propTypes = {
	product: PropTypes.shape({
		_id: PropTypes.string.isRequired,
		userId: PropTypes.string.isRequired,
		productName: PropTypes.string.isRequired,
		productBrand: PropTypes.string.isRequired,
		productDescription: PropTypes.string.isRequired,
		price: PropTypes.number.isRequired,
		stocksAvailable: PropTypes.number.isRequired,
		isActive: PropTypes.bool.isRequired,
		isApprovedByAdmin: PropTypes.bool.isRequired
	})
}