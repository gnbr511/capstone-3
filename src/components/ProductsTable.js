import {Table, Button, Modal, Form} from 'react-bootstrap'
import PropTypes from 'prop-types'
import Swal from 'sweetalert2'
import {Refresh} from '../pages/Dashboard'
import {useState} from 'react'

export default function ProductsTable({product}){
	const {_id,  productName, productBrand, productDescription, price, stocksAvailable, isActive, isApprovedByAdmin} = product
	const [modalShow, setModalShow] = useState(false)

	const [idToUpdate, setIdToUpdate] = useState('')
	const [prodName, setProdName] = useState('')
	const [prodBrand, setProdBrand] = useState('')
	const [prodDescription, setProdDescription] = useState('')
	const [prodPrice, setProdPrice] = useState(0)
	const [prodStock, setProdStock] = useState(0)

	const Formatter = new Intl.NumberFormat('en-US', {
		style: 'currency',
		currency: 'Php',
	})

	const DisableProduct = (id) => {
		fetch(`${process.env.REACT_APP_API_URL}/products/${id}/archive`, {
			method: 'PATCH',
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(response => response.json())
		.then(result => {			
			if(result.isSuccess){
				Swal.fire({
					title: 'Success',
					icon: 'success',
					confirmButtonColor: "#f0ad4e",
					text: result.message
				})
				Refresh()	
			}else{
				Swal.fire({
					title: 'Error',
					icon: 'error',
					confirmButtonColor: "#f0ad4e",
					text: result.message
				})
				Refresh()
			}
			
		})
	}

	const EnableProduct = (id) => {
		fetch(`${process.env.REACT_APP_API_URL}/products/${id}/unarchive`, {
			method: 'PATCH',
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(response => response.json())
		.then(result => {			
			if(result.isSuccess){
				Swal.fire({
					title: 'Success',
					icon: 'success',
					confirmButtonColor: "#f0ad4e",
					text: result.message
				})
				Refresh()	
			}else{
				Swal.fire({
					title: 'Error',
					icon: 'error',
					confirmButtonColor: "#f0ad4e",
					text: result.message
				})
				Refresh()
			}			
		})
	}

	const PassDataToModal = (_id, productName, productBrand, productDescription, price, stocksAvailable) => {
		setIdToUpdate(_id)
		setProdName(productName)
		setProdBrand(productBrand)
		setProdDescription(productDescription)
		setProdPrice(price)
		setProdStock(stocksAvailable)
		setModalShow(true)
	}

	const UpdateProduct = (event) => {
		event.preventDefault()


		let name = document.querySelector('#productName').value
		let brand = document.querySelector('#productBrand').value
		let description = document.querySelector('#productDescription').value
		let price = document.querySelector('#productPrice').value
		let stock = document.querySelector('#productStock').value
		if(price <= 0){
			document.querySelector('#productPrice').focus()
		}else if(stock <= 0){
			document.querySelector('#productStock').focus()
		}else{
			fetch(`${process.env.REACT_APP_API_URL}/products/${idToUpdate}/update`, {
				method: 'PATCH',
				headers: {
					'Content-Type': 'application/json',
					Authorization: `Bearer ${localStorage.getItem('token')}`
				},
				body: JSON.stringify({
					productName: name,
					productBrand: brand,
					productDescription: description,
					price: price,
					stocksAvailable: stock
				})
			})
			.then(response => response.json())
			.then(result => {
				if(result.isSuccess){
					Swal.fire({
						title: 'Success',
						icon: 'success',
						confirmButtonColor: "#f0ad4e",
						text: result.message
					})
					Refresh()
				}else{
					Swal.fire({
						title: 'Error',
						icon: 'error',
						confirmButtonColor: "#f0ad4e",
						text: result.message
					})
					Refresh()
				}
			})
		}

	}

	function UpdateProductModal (props){
		return (
		    <Modal
		      {...props}
		      size="md"
		      aria-labelledby="contained-modal-title-vcenter"
		      centered
		    >
		      <Modal.Header closeButton  className="bg-warning">
		        <Modal.Title id="contained-modal-title-vcenter">
		         <i className="fa-solid fa-pen-to-square"> Update Product</i>
		        </Modal.Title>
		      </Modal.Header>
		      <Form onSubmit={event => UpdateProduct(event)}>
		      	<Modal.Body>
		      		<Form.Label style={{fontWeight: 'bold'}}>Product Name</Form.Label>	      				    
		        	<Form.Control
		              type="text"
		              id="productName"
		              placeholder="Product name"	              
		              className="me-2"
		              defaultValue={productName}		              
		              aria-label="Search"
		              required	              
		            />

		            <Form.Label className="mt-3" style={{fontWeight: 'bold'}}>Product Brand</Form.Label>	      				    
		        	<Form.Control
		              type="text"
		              id="productBrand"
		              placeholder="Product brand"
		              className="me-2"
		              defaultValue={prodBrand}
		              aria-label="Search"
		              required	              
		            />

		            <Form.Label className="mt-2" style={{fontWeight: 'bold'}}>Product Description</Form.Label>	      				    
		        	<Form.Control
		              type="text"
		              id="productDescription"
		              placeholder="Product Description"
		              className="me-2"
		              defaultValue={prodDescription}
		              aria-label="Search"
		              required	              
		            />

		            <Form.Label className="mt-2" style={{fontWeight: 'bold'}}>Price</Form.Label>	      				    
		        	<Form.Control
		              type="number"
		              id="productPrice"
		              placeholder="Product price"
		              className="me-2"
		              defaultValue={prodPrice}
		              aria-label="Search"
		              required		              
		            />

		            <Form.Label className="mt-2" style={{fontWeight: 'bold'}}>Stock</Form.Label>	      				    
		        	<Form.Control
		              type="number"
		              id="productStock"
		              placeholder="Product stock"
		              className="me-2"
		              defaultValue={prodStock}
		              aria-label="Search"
		              required              
		            /> 
			    </Modal.Body>
			    <Modal.Footer>
			    	<Button variant="primary" type="submit">Update</Button>
			      	<Button id="modalCloseBtn" variant="danger" onClick={props.onHide}>Close</Button>
			    </Modal.Footer>
		      </Form>	      
		    </Modal>
		)
	}

	return(
		<tr>
			<td>{_id}</td>
			<td>{productName}</td>
			<td>{productBrand}</td>
			<td>{productDescription}</td>
			<td>{Formatter.format(price)}</td>
			<td>{stocksAvailable}</td>
			<td>{isActive ? 'Yes' : 'No'}</td>
			<td>{isApprovedByAdmin ? 'Yes' : 'No'}</td>			
			<td>
				<Button variant="primary" className="border-dark text-light" onClick={() => {
					PassDataToModal(_id, productName, productBrand, productDescription, price, stocksAvailable)
				}}>Edit</Button>
				<UpdateProductModal
					show={modalShow}
					onHide={() => setModalShow(false)}
				/>
			</td>
			<td>
				{isActive ?
					<Button variant="danger" onClick={() => DisableProduct(_id)} className="border-dark text-light">Disable</Button>
				:
					<Button variant="success" onClick={() => EnableProduct(_id)} className="border-dark text-light">Enable</Button>
				}				
			</td>
		</tr>
	)
}

ProductsTable.propTypes = {
	product: PropTypes.shape({
		_id: PropTypes.string.isRequired,
		userId: PropTypes.string.isRequired,
		productName: PropTypes.string.isRequired,
		productBrand: PropTypes.string.isRequired,
		productDescription: PropTypes.string.isRequired,
		price: PropTypes.number.isRequired,
		stocksAvailable: PropTypes.number.isRequired,
		isActive: PropTypes.bool.isRequired,
		isApprovedByAdmin: PropTypes.bool.isRequired
	})
}