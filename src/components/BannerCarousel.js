import {Carousel} from 'react-bootstrap'

export default function BannerCarousel(){
	return(
		<>
			<Carousel>					 
			  <Carousel.Item interval="3000">
			  		<i className="fa-solid"><h4>Buying something?</h4></i>
			      	<div className="carouselOpacity">
				        <img
				          className="d-block w-100 rounded"
				          src="https://gitlab.com/gnbr511/capstone3images/-/raw/master/buying-2.png"
				          alt="Buy?"
				        />
			        </div>
			  </Carousel.Item>
			  <Carousel.Item interval="3000">
			  	  <i className="fa-solid"><h4>Selling something?</h4></i>	
			      <div className="carouselOpacity">
				        <img
				          className="d-block w-100 rounded"
				          src="https://gitlab.com/gnbr511/capstone3images/-/raw/master/selling-1.png"
				          alt="Sell?"
				        />
			       </div>
			  </Carousel.Item>
			  <Carousel.Item interval="3000">
			  	<i className="fa-solid"><h4>Go with us, go with GOBAS!</h4></i>
			  	<div className="carouselOpacity">
			        <img
			          className="d-block w-100 rounded"
			          src="https://gitlab.com/gnbr511/capstone3images/-/raw/master/go-with-gobas-3.png"
			          alt="Go with GOBAS"
			        />
			    </div>
			  </Carousel.Item>
			</Carousel>
		</>
	)	
}